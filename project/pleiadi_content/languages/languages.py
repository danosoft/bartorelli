from django.conf import settings
from django.core.exceptions import ImproperlyConfigured


class Language(object):
    """
    LANGUAGES_CONFIGURATION is a settings key used to specify additional information for the languages of the site.

    Example:

    LANGUAGES_CONFIGURATION = {
        'en': {
            'locale': 'en',
            'active': True,
            'flag': 'images/languages/flags/uk.gif',
            'rel_alternate_language_code': 'x-default',
        },
        'it': {
            'locale': 'it-it',
            'active': True,
            'flag': 'images/languages/flags/it.gif',
            'rel_alternate_language_code': 'it',
        }
    }
    """
    available_languages = settings.LANGUAGES
    languages_configuration = getattr(settings, 'LANGUAGES_CONFIGURATION', {})

    # check settings configuration
    for language, label in available_languages:
        try:
            configuration = languages_configuration[language]
        except KeyError, e:
            raise ImproperlyConfigured("settings LANGUAGES_CONFIGURATION miss the KEY '%s'" % (language,))

    def __init__(self, code, locale, flag, active=False, rel_alternate_language_code=''):
        self.code = code
        self.locale = locale
        self.active = active
        self.flag = flag  # use a django file
        self.rel_alternate_language_code = rel_alternate_language_code  # use a django file

    def __unicode__(self):
        return u'%s' % self.code

    def __str__(self):
        return unicode(self).encode('utf-8')

    @classmethod
    def active_languages(cls):
        return [cls(lang_code,
                    language_configuration['locale'],
                    language_configuration['flag'],
                    language_configuration['active'],
                    language_configuration['rel_alternate_language_code']
                    )
                for lang_code, language_configuration in cls.languages_configuration.iteritems()
                if language_configuration['active']]

    @classmethod
    def available_languages(cls):
        return [cls(lang_code,
                    language_configuration['locale'],
                    language_configuration['flag'],
                    language_configuration['active'],
                    language_configuration['rel_alternate_language_code'])
                for lang_code, language_configuration in cls.languages_configuration.iteritems()]

    @classmethod
    def get_configuration(cls, language_code):
        for language in cls.available_languages():
            if language.code == language_code:
                return language

        return None
