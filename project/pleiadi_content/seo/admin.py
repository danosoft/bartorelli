class SeoAdminMixin(object):
    """
    Defines constants for fieldsets and list_display concerning SEO data of the Model instance.
    """
    seo_fieldset = ('SEO', {
      'fields': ('seo_title', 'seo_description', ),
      'classes': ('collapse',),
    })