from pleiadi_content.seo.translation import SeoTranslationOptions
from modeltranslation.translator import TranslationOptions


class VisibilityTranslationOptions(TranslationOptions):
    fields = ('visible', )


class BaseTranslationOptions(SeoTranslationOptions, VisibilityTranslationOptions):
    fields = ('title', 'slug', 'description', 'abstract', 'image_alt')
    empty_values = {'slug': ''}


class DateValidityBaseTranslationOption(BaseTranslationOptions):
    pass
