from modeltranslation.translator import TranslationOptions


class BaseModelTranslationOptions(TranslationOptions):
    """
    Translation options for pleiadi_cmsplugins_base model.

    If your model extends BaseModel, your TranslationOption class may extend this. Otherwise your TranslationOption
    class must define BaseModel fields explicitly as always.
    """
    fields = ('title', 'slug')
    empty_values = {'slug': ''}
