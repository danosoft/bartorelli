# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-02 15:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='id',
        ),
        migrations.AlterField(
            model_name='product',
            name='xml_id',
            field=models.CharField(max_length=255, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
