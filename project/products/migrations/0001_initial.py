# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-02-02 13:13
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Gender',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, verbose_name='Name')),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('xml_id', models.CharField(max_length=255, verbose_name='ID')),
                ('title', models.TextField(verbose_name='Title')),
                ('short_title', models.TextField(verbose_name='Short title')),
                ('abstract', models.TextField(verbose_name='Abstract')),
                ('gender', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='products', to='products.Gender')),
            ],
        ),
    ]
