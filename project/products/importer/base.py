class Importer(object):
    def __init__(self, source_path, **kwargs):
        self.source_path = source_path

    def parse(self, **kwargs):
        raise NotImplementedError
