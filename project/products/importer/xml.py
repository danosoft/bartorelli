import untangle

from products.importer.base import Importer


class XmlImporter(Importer):
    def parse(self):
        xml_file_path = self.source_path
        return untangle.parse(xml_file_path)

    def get_node(self, parsed_source, path):
        node = parsed_source

        for step in path.split('/'):
            node = getattr(node, step)

        return node

# class ProductImporter(XmlImporter):
#     def get_products(self):
#         products_to_import = self.get_node(self.parse(), 'EasyCommerceExport/Products/Product')
#
#
#         return products_to_import
