from django.contrib import admin
from products.models import Product, Gender, Movement

admin.site.register(Product)
admin.site.register(Gender)
admin.site.register(Movement)
