from products.importer.xml import XmlImporter
from products.models import Product, Gender, WatchStrap, Material, Movement, WatchType, Brand, Collection


class ProductImporter(XmlImporter):
    model = Product

    STATUS_NOP = 'nop'

    STATUS_NEW = 'new'
    STATUS_UPDATE = 'update'
    STATUS_REMOVE = 'remove'

    STATUS_CONFIGURATION = {
        STATUS_NOP: {
            'css_class': '',
            'label': '',
        },
        STATUS_NEW: {
            'css_class': 'success',
            'label': 'New',
        },
        STATUS_UPDATE: {
            'css_class': 'warning',
            'label': 'Updated',
        },
        STATUS_REMOVE: {
            'css_class': 'danger',
            'label': 'Removed',
        },
    }

    def get_locale_property(self, item, key, fallback=''):
        locale_labels = item.Local.Label
        try:
            return [label.cdata for label in locale_labels if label['Key'] == key][0]
        except IndexError:
            return fallback

    def get_fk_property(self, model, property_value, save=False):
        try:
            return model.objects.get(name=property_value)
        except model.DoesNotExist:
            instance = model(name=property_value)
            if save:
                instance.save()
            return instance

    def check_product_status(self, old_product, new_product, fields):
        compared_fields = {}
        if not old_product:
            # the product is new
            status = self.STATUS_NEW
            compared_fields = {
                field: {
                    'value': getattr(new_product, field),
                    'updated': self.STATUS_NOP
                } for field in fields}
        else:
            # the product will be updated
            status = self.STATUS_NOP
            for field in fields:
                if getattr(old_product, field) != getattr(new_product, field):
                    field_status = self.STATUS_UPDATE
                    status = self.STATUS_UPDATE
                else:
                    field_status = self.STATUS_NOP

                compared_fields.update({
                    field: {
                        'value': getattr(new_product, field),
                        'import_status': field_status,
                        'import_status_template': self.STATUS_CONFIGURATION[field_status]
                    }})

        return status, compared_fields

    def get_products(self, save=False):
        products_to_import = self.get_node(self.parse(), 'EasyCommerceExport/Products/Product')

        products = []
        # for every xml record I need to parse and check different models
        for xml_product in products_to_import:
            # Brand
            brand = self.get_fk_property(Brand, xml_product['Brand'], save)

            # Collection
            collection = self.get_fk_property(Collection, self.get_locale_property(xml_product, 'ClCOL'), save)

            # Gender
            gender = self.get_fk_property(Gender, self.get_locale_property(xml_product, 'ClSZ'), save)

            # WatchStrap
            watch_strap = self.get_fk_property(WatchStrap, self.get_locale_property(xml_product, 'ClBS'), save)

            # Material
            material = self.get_fk_property(Material, self.get_locale_property(xml_product, 'ClCM'), save)

            # Movement
            movement = self.get_fk_property(Movement, self.get_locale_property(xml_product, 'ClMO'), save)

            # WatchType
            watch_type = self.get_fk_property(WatchType, self.get_locale_property(xml_product, 'ClPMO'), save)

            # Product (the watch itself)
            xml_id = xml_product['ID']
            title = self.get_locale_property(xml_product, 'Title')
            short_title = self.get_locale_property(xml_product, 'ShortTitle')
            abstract = self.get_locale_property(xml_product, 'Abstract')
            product = Product(
                xml_id=xml_id,
                title=title,
                short_title=short_title,
                abstract=abstract,
                gender=gender,
                watch_strap=watch_strap,
                material=material,
                movement=movement,
                watch_type=watch_type,
                brand=brand,
                collection=collection)

            products.append(product)

        return products

    def check_import(self):
        products_to_import = self.get_products()

        products_check = []
        for product_to_import in products_to_import:

            try:
                existing_product = Product.objects.get(xml_id=product_to_import.xml_id)
            except Product.DoesNotExist:
                existing_product = None

            fields_to_compare = ['xml_id', 'brand', 'collection', 'title', 'short_title', 'abstract', 'gender',
                                 'watch_strap', 'material', 'movement', 'watch_type']
            product_status, field_status = self.check_product_status(existing_product, product_to_import,
                                                                     fields_to_compare)
            products_check.append({
                'import_status': product_status,
                'import_status_template': self.STATUS_CONFIGURATION[product_status],
                'fields': field_status
            })

        return products_check

    def import_products(self):
        products_to_import = self.get_products(save=True)

        errors = []
        for product_to_import in products_to_import:
            try:
                product_to_import.save()
            except Exception:
                errors.append('Unable to import product: %s' % product_to_import.title)

        # TODO: purge related models where .products.count() == 0

        return errors

    def clean_unused_related(self):
        pass
