import os

from django.shortcuts import render

from products.product_importer import ProductImporter


def check_import(request):
    xml_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'import_data/RICHARDMILLE_example.xml')

    importer = ProductImporter(xml_file_path)

    products = importer.check_import()

    context = {
        'products': products,
        'total_products': len(products),
        'new_products': len(
            [product for product in products if product['import_status'] == ProductImporter.STATUS_NEW]),
        'updated_products': len(
            [product for product in products if product['import_status'] == ProductImporter.STATUS_UPDATE]),
    }

    return render(request, 'products/importer/products_import_preview.html', context)


def import_products(request):
    xml_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'import_data/RICHARDMILLE_example.xml')

    importer = ProductImporter(xml_file_path)

    errors = importer.import_products()

    context = {
        'errors': errors
    }

    return render(request, 'products/importer/products_import_result.html', context)
