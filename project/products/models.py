from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


class ProductFkPropertyModel(models.Model):
    name = models.CharField(_('Name'), max_length=255)

    class Meta:
        abstract = True

    def __unicode__(self):
        return u'%s' % self.name


class Gender(ProductFkPropertyModel):
    pass


class WatchStrap(ProductFkPropertyModel):
    pass


class Material(ProductFkPropertyModel):
    pass


class Movement(ProductFkPropertyModel):
    pass


class WatchType(ProductFkPropertyModel):
    pass


class Brand(ProductFkPropertyModel):
    pass


class Collection(ProductFkPropertyModel):
    pass


class Product(models.Model):
    xml_id = models.CharField(_('ID'), max_length=255, primary_key=True)

    title = models.TextField(_('Title'), blank=True, null=True)
    short_title = models.TextField(_('Short title'), blank=True, null=True)
    abstract = models.TextField(_('Abstract'), blank=True, null=True)

    brand = models.ForeignKey(Brand, related_name='products', blank=True, null=True)
    collection = models.ForeignKey(Collection, related_name='products', blank=True, null=True)

    gender = models.ForeignKey(Gender, related_name='products', blank=True, null=True)
    watch_strap = models.ForeignKey(WatchStrap, related_name='products', blank=True, null=True)
    material = models.ForeignKey(Material, related_name='products', blank=True, null=True)
    movement = models.ForeignKey(Movement, related_name='products', blank=True, null=True)
    watch_type = models.ForeignKey(WatchType, related_name='products', blank=True, null=True)
