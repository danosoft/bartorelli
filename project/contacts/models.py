# -*- coding: utf-8 -*-
from django.db import models
from pleiadi_content.content.models import AdministrativeMixin
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

class Contact(AdministrativeMixin):
    CONTACT_TYPE_SERV = (
        ('reg', _('REGISTRAZIONE')),
        ('cess', _('CESSAZIONE')),
        ('mod', _('MODIFICA')),
        ('sub', _('SUBENTRO')),
        ('proro', _('PROROGA'))
    )

    CONTACT_TYPE_CONTR = (
        ('prv', _('PRIVATO')),
        ('com', _('COMMERCIALE'))
    )

    CONTACT_TYPE_PAGA = (
        ('bon', _('Bonifico')),
        ('ppc', _('Paypal/Carta di Credito'))
    )

    CONTACT_TYPE_CHOICES = (
        ('abi', _('Ad uso abitativo')),
        ('abicoage', _('Ad uso abitativo concordato-agevolato')),
        ('abiiva', _('Ad uso abitativo soggetto ad iva')),
        ('divabi', _('Ad uso diverso dall\'abitazione')),
        ('divabistriva', _('Ad uso diverso dall\'abitazione di immobile strumentale con locatore soggetto ad iva'))
    )

    YES_OR_NO = (
        ('Si', 'Si'),
        ('No', 'No')
    )

    nome_registrazione = models.CharField(_('Nome registrazione'), max_length=255, blank=False, null=False)
    datastipulacontratto = models.DateTimeField()
    datainiziolocazione = models.DateTimeField()
    dataprimascadenzacontratto = models.DateTimeField()
    tiposervizio = models.CharField(_('Tipologia di Servizio'), max_length=255, blank=False, null=False,choices = CONTACT_TYPE_SERV)
    tipocontr = models.CharField(_('Tipologia di Contratto'), max_length=255, blank=False, null=False,choices=CONTACT_TYPE_CONTR)
    tipocontratto = models.CharField(_('Tipologia di Contratto due'), max_length=255, blank=False, null=False, choices=CONTACT_TYPE_CHOICES)
    canonetot = models.DecimalField(_('Canone totale solo dei primi 12 mesi Euro (separatore decimale il "." '), max_digits=10, decimal_places=2, blank=True, null=True)
    elecodfislocatori = models.TextField(_('Elenco codici fiscale locatori (uno per riga)'), blank=True, null=True)
    elecodfislocatari = models.TextField(_('Elenco codici fiscale locatari (uno per riga)'), blank=True, null=True)

    # garanzie = models.CharField(_('Garanzie'), max_length=10, blank=True, null=True, choices=YES_OR_NO)
    # dati catastali

    classe = models.CharField(_('Classe'), max_length=10, blank=True, null=True)
    rendita = models.DecimalField(_('Rendita'), max_digits=10, decimal_places=2, blank=True, null=True)
    sezione = models.CharField(_('Sezione'), max_length=10, blank=True, null=True)
    foglio = models.CharField(_('Foglio'), max_length=10, blank=True, null=True)
    particella = models.CharField(_('Particella'), max_length=10, blank=True, null=True)
    subalterno = models.CharField(_('Subalterno'), max_length=10, blank=True, null=True)
    categoria = models.CharField(_('Categoria'), max_length=10, blank=True, null=True)
    comune = models.CharField(_('Comune'), max_length=10, blank=True, null=True)

    first_name = models.CharField(_('Nome'), max_length=255, blank=False, null=False)
    last_name = models.CharField(_('Cognome'), max_length=255, blank=False, null=False)
    email = models.EmailField(_('Email'), blank=False, null=False)
    city = models.CharField(_('Comune'), max_length=255, blank=False, null=False)
    tel = models.CharField(_('Telefono'), max_length=255, blank=True, null=True)
    message = models.TextField(_('Messaggio'), blank=True, null=True)


    contratto = models.FileField(_('Allega il contratto (formato .doc o .pdf)'), upload_to='contacts/contratti', blank=True, null=True)
    visura = models.FileField(_('Allega la visura (formato .doc o .pdf)'), upload_to='contacts/contratti', blank=True, null=True)


    privacy_ok = models.BooleanField(_('privacy_accept'))
    newsletter = models.BooleanField(_('newsletter_accept'), blank=True)
    pagato = models.BooleanField(_('pagato'), blank=True, default=False)
    calcolodovuto = models.DecimalField(_('Calcolo del dovuto'), max_digits=10, decimal_places=2, blank=True, null=True)
    modalitapagamento = models.CharField(_('Modalita di pagamento'), max_length=255, blank=False, null=False, choices=CONTACT_TYPE_PAGA)

    nrprotoade = models.CharField(_('Protocollo Ade'), max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = _('Contact')
        verbose_name_plural = _('Contacts')
        ordering = ('dataprimascadenzacontratto', )


    def __unicode__(self):
        return '%s %s' % (self.first_name, self.last_name, )