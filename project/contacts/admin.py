from django.contrib import admin
from import_export.admin import ExportMixin

from contacts.models import Contact
from contacts.resources import ContactExportResource
from pleiadi_content.base.admin import ReadOnlyFieldsMixin
from django.db import models

# class AfterSendAdmin(admin.ModelAdmin):
#     def save_model(self, obj, nrprotoade):
#         obj.nrprotoade = request.nrprotoade
#         obj.save()

class BranchAdminVisibilityMixin(object):
    change_form_template = 'admin_overrides/branch_admin_visibility/change_form.html'

class ContactAdmin(BranchAdminVisibilityMixin, ExportMixin, admin.ModelAdmin):
    # ReadOnlyFieldsMixin,
    resource_class = ContactExportResource
    list_display = ('first_name', 'last_name', 'email','pagato', 'calcolodovuto', 'dataprimascadenzacontratto', 'tiposervizio','nome_registrazione', 'nrprotoade', 'creation_date')
    date_hierarchy = 'creation_date'
    search_fields = ('pagato', 'tiposervizio', 'tipocontr', 'dataprimascadenzacontratto')
    list_filter = ['pagato']
    exclude = ()

    fieldsets = (
        ('Dati Generici', {
            'fields': (
                'nome_registrazione',
                'first_name',
                'last_name',
                'datastipulacontratto',
                'datainiziolocazione',
                'dataprimascadenzacontratto',
                'tiposervizio',
                'tipocontr',
                'tipocontratto',
                'canonetot',
                'email',
                'message',
                'elecodfislocatari',
                'elecodfislocatori',
                'city',
                'tel',
                'contratto',
                'visura',
                'classe',
                'rendita',
                'sezione',
                'foglio',
                'particella',
                'subalterno',
                'categoria',
                'comune',
                'newsletter',
                'privacy_ok',
                'calcolodovuto',
                'pagato',
            ),
            'classes': ('collapse',),
        }),
        ('Created', {
            'fields': (
                'created_by',
            ),
            'classes': ('collapse',),
        }),
        ('Protocollo AdE', {
            'fields': (
                'nrprotoade',
            )
        }),
    )

admin.site.register(Contact, ContactAdmin)
