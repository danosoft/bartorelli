from django import forms
from django.forms.widgets import SelectDateWidget
from django.utils.html import mark_safe
from contacts.models import Contact
from django.utils.translation import ugettext_lazy as _

class HorizRadioRenderer(forms.RadioSelect.renderer):
    """ this overrides widget method to put radio buttons horizontally
        instead of vertically.
    """
    def render(self):
            """Outputs radios"""
            return mark_safe(u'\n'.join([u'%s\n' % w for w in self]))

class ContactForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        self.user = self.request.user
        super(ContactForm, self).__init__(*args, **kwargs)
        # self.fields['datainiziolocazione'] = forms.DateTimeField(label="aaaaaaa", required=True)
        self.fields['tiposervizio'] = forms.ChoiceField(widget=forms.Select(attrs={"onChange": 'tiposerviziox(this)'}), choices=Contact.CONTACT_TYPE_SERV)

        self.fields['tipocontratto'] = forms.ChoiceField(widget=forms.RadioSelect, choices=Contact.CONTACT_TYPE_CHOICES)
        self.fields['calcolodovuto'].widget = forms.HiddenInput()
        # self.fields['canonetot'] = forms.CharField(label=_('Canone totale solo dei primi 12 mesi Euro* (come separatore decimale usare il "." '), required=True)

        self.fields['datainiziolocazione'].widget.attrs['readonly'] = True
        self.fields['datastipulacontratto'].widget.attrs['readonly'] = True
        self.fields['dataprimascadenzacontratto'].widget.attrs['readonly'] = True


        # self.fields['datainiziolocazione'] = forms.DateInput(
        #     widget=SelectDateWidget(
        #         empty_label=("Anno", "Mese", "Giorno"),
        #     ),
        # )
        # self.fields['datastipulacontratto'] = forms.DateField(
        #     widget=SelectDateWidget(
        #         empty_label=("Anno", "Mese", "Giorno"),
        #     ),
        # )
        # self.fields['dataprimascadenzacontratto'] = forms.DateField(
        #     widget=SelectDateWidget(
        #         empty_label=("Anno", "Mese", "Giorno"),
        #     ),
        # )

        # self.fields['garanzie'] = forms.ChoiceField(widget=forms.RadioSelect, choices=Contact.YES_OR_NO)

        # self.fields['tipocontratto'] = forms.CharField(label="Tipologia di Contratto due",
        #                                                widget=forms.RadioSelect(renderer=HorizRadioRenderer,choices=Contact.CONTACT_TYPE_CHOICES), required=True)


    class Meta:
        model = Contact
        fields = ('first_name', 'last_name', 'nome_registrazione',
                  'datastipulacontratto', 'datainiziolocazione', 'dataprimascadenzacontratto',
                  'tiposervizio', 'tipocontr', 'tipocontratto',
                  'canonetot', 'email', 'elecodfislocatori', 'message', 'elecodfislocatari', 'city', 'tel', 'contratto', 'visura',
                  'newsletter', 'privacy_ok', 'modalitapagamento', 'calcolodovuto',
                  'classe', 'rendita', 'sezione', 'foglio', 'particella', 'subalterno', 'categoria', 'comune', )
        exclude = ()

    # def clean(self):
    #     shipping = self.cleaned_data.get('tiposervizio')
    #     if shipping:
    #         self.fields_required(['canonetot','elecodfislocatori', 'elecodfislocatari', ])
    #     else:
    #         self.cleaned_data['shipping_destination'] = ''
    #     return self.cleaned_data
    #
    # def fields_required(self, fields):
    #     """Used for conditionally marking fields as required."""
    #     for field in fields:
    #         if not self.cleaned_data.get(field, ''):
    #             msg = forms.ValidationError("This field is required.")
    #             self.add_error(field, msg)

    def save(self, commit=True):
        instance = super(ContactForm, self).save(commit=False)

        instance.created_by = self.user
        instance.changed_by = self.user
        if commit:
            instance.save()
        return instance