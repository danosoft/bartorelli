from django.conf.urls import patterns, url
from contacts import views

urlpatterns = patterns('',
    url(r'^$', 'contacts.views.contact_reg', name='contact_reg'),
)