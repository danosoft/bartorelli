from django.utils.translation import ugettext_lazy as _
from django.db import models
from cms.models import CMSPlugin
from filer.fields.image import FilerFileField

from cmsplugin_filer_utils import FilerPluginManager
from pleiadi_cmsplugins_base.fields import DimensionField

from .conf import settings
from pleiadi_cmsplugins_base.models import LinkMixin, VideoMixin
from pleiadi_content.base.models.fields import HtmlTextField


class Teaser(CMSPlugin, LinkMixin, VideoMixin):
    """
    A Teaser
    """
    TEASER_STYLE_CHOICES = settings.PLEIADI_CMSPLUGINS_TEASER_STYLE_CHOICES

    title = models.CharField(_("title"), max_length=255, blank=True)
    media = FilerFileField(verbose_name=_('Main/Background Image'), null=True, blank=True,
                           help_text=_('Set a main image for your pleiadi_cmsplugins_teaser; It will be shown according to the selected'))
    description = HtmlTextField(_("description"), blank=True, null=True)
    style = models.CharField(_('Teaser Template'), max_length=255, choices=TEASER_STYLE_CHOICES,
                             default=settings.PLEIADI_CMSPLUGINS_TEASER_DEFAULT_STYLE)
    width = DimensionField(_('max. width'), null=True, blank=True,
                           help_text=_('You can leave this empty to use an automatically determined image width.'), default=1024)
    height = DimensionField(_('max. height'), null=True, blank=True,
                            help_text=_('You can leave this empty to use an automatically determined image height.'
                                        '<br /><strong>Width</strong> is intended to be 100% of the container'))

    objects = FilerPluginManager(select_related=('media',))

    def __unicode__(self):
        return self.title

    @property
    def template(self):
        return 'pleiadi_cmsplugins_teaser/teaser_%s.html' % (self.style, )

    @property
    def dimension(self):
        if self.width and self.height:
            return '%sx%s' % (self.width, self.height)

        return None
