from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase
from django.utils.translation import ugettext_lazy as _

from pleiadi_cmsplugins_base.cms_plugins import LinkMixinPlugin
from pleiadi_cmsplugins_teaser.models import Teaser


class TeaserPlugin(LinkMixinPlugin, CMSPluginBase):
    model = Teaser
    name = _("Teaser")
    render_template = 'pleiadi_cmsplugins_base/empty_fake.html'

    fieldsets = (
        (None, {
            'fields': ('style',)
        }),
        (_('Contents'), {
            'classes': ('collapse',),
            'fields': ('title', 'description', 'media', ('width', 'height'))
        }),
        ('link', {
            'classes': ('collapse',),
            'fields': (('link_type', 'link_target'), 'link_page', 'link_url')
        }),
        ('video', {
            'classes': ('collapse',),
            'fields': (('video_type', 'video_id'), ('video_width', 'video_height'))
        }),
    )

    def render(self, context, instance, placeholder):
        context = super(TeaserPlugin, self).render(context, instance, placeholder)

        self.render_template = getattr(instance, 'template', 'pleiadi_cmsplugins_base/empty_fake.html')

        context.update({
            'instance': instance,
            'title': instance.title,
            'description': instance.description,
            'media': instance.media,
            'link_to': instance.link_to,
            'width': instance.width,
            'height': instance.height,
            'dimension': instance.dimension,

        })

        return context
plugin_pool.register_plugin(TeaserPlugin)
