from django.conf import settings
from appconf import AppConf
from django.utils.translation import ugettext_lazy as _


class TeaserConf(AppConf):
    STYLE_CHOICES = (
        ('left_image', _('Left Image')),
        ('right_image', _('Right Image')),
        ('banner', _('Banner')),
        ('video', _('Video')),
        ('image', _('Image')),
    )
    DEFAULT_STYLE = 'left_image'

    class Meta:
        prefix = 'pleiadi_cmsplugins_teaser'