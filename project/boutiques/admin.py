from django.contrib import admin

from pleiadi_content.content.admin import DateValidityBaseContentAdmin
from boutiques.models import Boutiques, Media
from modeltranslation.admin import TranslationTabularInline

from django.utils.translation import ugettext_lazy as _



class MediaAdmin(TranslationTabularInline):
    model = Media
    extra = 0


class BoutiquesAdmin(DateValidityBaseContentAdmin):
    """
    list_display for Admin subclasses is static for now
    """
    content_fieldset = (None, {
        'fields': ('title', 'slug', 'abstract', 'description', 'image', 'image_alt', 'link_dovesiamo', 'image_dovesiamo')
    })
    list_display = ['admin_title', 'active', 'valid_from', 'valid_to', 'translations', 'order']
    inlines = [MediaAdmin, ]
    list_editable = ('order',)

    class Media:
        css = {
            'all': (
                'stylesheets/admin_languages.css',
            )
        }


admin.site.register(Boutiques, BoutiquesAdmin)
