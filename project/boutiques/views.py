from django.core.urlresolvers import resolve
from django.shortcuts import render, get_object_or_404, render_to_response
from django.template import RequestContext
from boutiques.models import Boutiques


def boutiques_detail(request, boutiques_slug):
    current_app = resolve(request.path_info).namespace
    context = RequestContext(request, current_app=current_app)
    template = 'boutiques/boutiques_detail.html'

    boutiques = get_object_or_404(Boutiques.visible_on_site.filter(slug=boutiques_slug))

    context.update({
        'boutiques': boutiques
    })

    setattr(request, 'rel_alternate', boutiques.rel_alternates(current_app=current_app))
    return render_to_response(template, context_instance=context)
