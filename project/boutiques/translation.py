from modeltranslation.translator import translator, TranslationOptions
from pleiadi_content.content.translation import DateValidityBaseTranslationOption
from pleiadi_content.base.translations import BaseModelTranslationOptions
from boutiques.models import Boutiques, Media


translator.register(Boutiques, DateValidityBaseTranslationOption)

class MediaTranslationOptions(TranslationOptions):
    fields = ('title', )
translator.register(Media, MediaTranslationOptions)

