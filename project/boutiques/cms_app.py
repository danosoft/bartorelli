from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _
from boutiques.menu import BoutiquesMenu

class BoutiquesHook(CMSApp):
    name = _("Boutiques")
    urls = ["boutiques.urls"]
    menus = [BoutiquesMenu]
    app_name = "boutiques"

apphook_pool.register(BoutiquesHook)
