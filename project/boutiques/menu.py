from menus.base import NavigationNode
from cms.menu_bases import CMSAttachMenu
from menus.menu_pool import menu_pool
from django.utils.translation import ugettext_lazy as _
from boutiques.models import Boutiques
from django.core.urlresolvers import reverse


class BoutiquesMenu(CMSAttachMenu):
    name = _("Boutiques Menu")

    def get_nodes(self, request):
        return get_nodes(self, request)


menu_pool.register_menu(BoutiquesMenu)


def get_nodes(instance, request):
    progetti = Boutiques.objects.all().order_by('title')

    nodes = []
    for item in progetti:
        item_menu_id = 'menubout_%s' % (item.pk,)
        reverse_url = reverse('boutiques:boutiques_detail', kwargs={
            'boutiques_slug': item.slug,
        })

        menu_node = NavigationNode(
            item.title,
            reverse_url,
            item_menu_id
        )

        menu_node.hide_in_site = True
        nodes.append(menu_node)

    return nodes
