from django.core.urlresolvers import reverse
from django.db import models
from django.utils.html import strip_tags
from django.utils.translation import ugettext_lazy as _
from pleiadi_content.content.models import BaseContentDateValidity
from filer.fields.file import FilerFileField
from filer.fields.image import FilerImageField


class Boutiques(BaseContentDateValidity):
    thumbnail_size = "424x282"
    order = models.IntegerField('order', default=1)

    image_dovesiamo = FilerImageField(
        null=True,
        blank=True,
        related_name="+",
        verbose_name=_('Immagine dove siamo (width:270x390)')
    )
    link_dovesiamo = models.CharField(_('link_dovesiamo'), max_length=250, blank=True, null=True,
                                      help_text=_('Link Dove siamo'))

    objects = models.Manager()

    class Meta:
        verbose_name = _('Boutiques')
        verbose_name_plural = _('Boutiques')
        ordering = ('order', 'title',)

    def __unicode__(self):
        return u'%s' % self.title

    @property
    def title(self):
        return self.title

    def get_absolute_url(self, current_app=None):
        current_app = current_app or 'boutiques'

        return reverse('%s:detail' % current_app, args=[self.slug])

    def get_seo_fallback_title(self):
        return strip_tags(self.title) if self.title else ''

    def get_seo_fallback_description(self):
        return strip_tags(self.description) if self.description else ''


class Media(models.Model):
    """
    Model for the inline image/video gallery. If video-code is not empty the file become the thumbnail for the video.


    Media
    file: handled by filer, thumbnail for image-zoom or video
    video_code: the full url of the video
    """
    MEDIA_TYPE_VIDEO = 'video'
    MEDIA_TYPE_IMAGE = 'image'
    MEDIA_TYPE_NOMEDIA = None

    article = models.ForeignKey(Boutiques, related_name='media')

    title = models.CharField(_('title'), max_length=250, blank=True, null=True, help_text=_('Image alt/title'))
    file = FilerFileField(related_name="+", blank=True, null=True,
                          help_text=_('If you are adding a video, use this field to upload a video icon.'))
    video_code = models.CharField(_('Video Url'), max_length=512, blank=True, null=True,
                                  help_text=_('Past here the url uf your video.'))
    order = models.PositiveIntegerField(_('order'), default=99999999)

    class Meta:
        verbose_name = _('Media')
        verbose_name_plural = _('Media')
        ordering = ['order']

    def __unicode__(self):
        return u"%s" % self.get_title()

    def get_title(self):
        return self.title or self.file

    @property
    def media_url(self):
        return self.video_code or self.file.url

    @property
    def media_type(self):
        if self.video_code:
            return self.MEDIA_TYPE_VIDEO

        return self.MEDIA_TYPE_IMAGE
