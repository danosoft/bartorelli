from django.conf.urls import patterns, url

urlpatterns = patterns('',
     url(r'^(?P<boutiques_slug>[\w+-]*)/$', 'boutiques.views.boutiques_detail', name='boutiques_detail'),
)