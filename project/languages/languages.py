from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import get_language

class Language(object):
    """
    LANGUAGES_CONFIGURATION is a settings key used to specify additional information for the languages of the site.

    Example:

    LANGUAGES_CONFIGURATION = {
        'en': {
            'locale': 'en',
            'active': True,
            'flag': 'images/languages/flags/uk.gif',
            'rel_alternate_language_code': 'x-default',
        },
        'it': {
            'locale': 'it-it',
            'active': True,
            'flag': 'images/languages/flags/it.gif',
            'rel_alternate_language_code': 'it',
        }
    }
    """
    available_languages = settings.LANGUAGES
    languages_configuration = getattr(settings, 'LANGUAGES_CONFIGURATION', {})

    # check settings configuration
    for language, label in available_languages:
        try:
            configuration = languages_configuration[language]
        except KeyError, e:
            raise ImproperlyConfigured("settings LANGUAGES_CONFIGURATION miss the KEY '%s'" % (language,))

    def __init__(self, code, label,locale, flag, active=False, rel_alternate_language_code='', country_slug=''):
        self.code = code
        self.label = label
        self.locale = locale
        self.active = active
        self.flag = flag  # use a django file
        self.rel_alternate_language_code = rel_alternate_language_code  # use a django file
        self.country_slug = country_slug

    def __unicode__(self):
        return u'%s' % self.code

    def __str__(self):
        return unicode(self).encode('utf-8')

    @property
    def selected(self):
        return self.code == get_language()

    @classmethod
    def active_languages(cls):
        return [cls(lang_code,
                    language_configuration['label'],
                    language_configuration['locale'],
                    language_configuration['flag'],
                    language_configuration['active'],
                    language_configuration['rel_alternate_language_code'],
                    language_configuration['country_slug']
                    )
                for lang_code, language_configuration in cls.languages_configuration.iteritems()
                if language_configuration['active']]

    @classmethod
    def not_active_languages(cls):
        return [language for language in cls.available_languages() if language.code not in [
            active_language.code for active_language in cls.active_languages()]]

    @classmethod
    def available_languages(cls):
        return [cls(lang_code,
                    language_configuration['label'],
                    language_configuration['locale'],
                    language_configuration['flag'],
                    language_configuration['active'],
                    language_configuration['rel_alternate_language_code'],
                    language_configuration['country_slug'])
                for lang_code, language_configuration in cls.languages_configuration.iteritems()]

    @classmethod
    def get_configuration(cls, language_code):
        for language in cls.available_languages():
            if language.code == language_code:
                return language

        return None
