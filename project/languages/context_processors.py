from languages import Language
from django.utils.translation import get_language


def languages(request):
    return {
        'available_languages': Language.available_languages(),
        'active_languages': Language.active_languages(),
        'not_active_languages': Language.not_active_languages(),
        'current_language': Language.get_configuration(get_language())
    }
