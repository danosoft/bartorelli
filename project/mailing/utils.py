from django.utils.translation import ugettext_lazy as _
from django.db.models.fields import BooleanField
from django.core.mail import EmailMultiAlternatives, get_connection


def get_data_table(form, fields, exclude):
    data_table = []
    for field in fields:
        if field.name not in exclude:
            value = form.cleaned_data[field.name]
            if field.choices:
                value = getattr(form.instance, 'get_%s_display' % field.name)()
            if isinstance(field, BooleanField):
                value = _('Yes') if value else _('No')
            data_table.append({
                'label': field.verbose_name,
                'value': unicode(value),
                'field': field.name
            })

    return data_table


def send_email(from_email, recipients, subject, body, cc=None, bcc=None, replyto=None, allegati=None):
    connection = get_connection(fail_silently=False)

    message = EmailMultiAlternatives(
        subject,
        body,
        from_email=from_email,
        to=recipients,
        bcc=bcc,
        cc=cc,
        connection=connection,
        headers={'Reply-To': replyto}
    )
    message.content_subtype = "html"
    if allegati:
        for item in allegati:
            message.attach_file(item)
    return message.send()


def send_courtesy_email():
    pass

