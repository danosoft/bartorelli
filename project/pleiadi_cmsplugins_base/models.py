import os

from django.db import models
# from django.conf import settings
from cms.models import CMSPlugin
from cms.models.fields import PageField
from cmsplugin_filer_utils import FilerPluginManager
from django.utils.translation import ugettext_lazy as _
from filer.fields.file import FilerFileField
from pleiadi_content.base.models.fields import HtmlTextField
from pleiadi_cmsplugins_base.conf import settings
from pleiadi_cmsplugins_base.fields import DimensionField


class ListBase(CMSPlugin):
    """
    Base model for all listing plugins: FileList, ImageGallery, Slideshow, ecc...

    It takes care of the copy_relations issue.

    All the listing plugins models (those with related items) MUST extend this class.
    """
    title = models.CharField(_("title"), max_length=255, null=True, blank=True)
    description = HtmlTextField(_('description'), max_length=255, blank=True, null=True,
                                help_text=_('Set a description for your pleiadi_cmsplugins_gallery; It will be shown according to the '
                                            'selected template.'))
    media = FilerFileField(verbose_name=_('Main/Background Image'), null=True, blank=True,
                           help_text=_('Set a main image for your pleiadi_cmsplugins_gallery; It will be shown according to the selected '
                                       'template (Usefull as pleiadi_cmsplugins_gallery background).'))

    objects = FilerPluginManager()

    def __unicode__(self):
        return self.title

    class Meta:
        abstract = True
        verbose_name = _('filer list')
        verbose_name_plural = _('filer lists')

    def copy_relations(self, old_instance):
        for associated_item in old_instance.media_items.all():
            # instance.pk = None; instance.pk.save() is the slightly odd but
            # standard Django way of copying a saved model instance
            associated_item.pk = None
            associated_item.container_list = self
            associated_item.save()


#
#   Mixin
#
class LinkMixin(models.Model):
    """
    Fields and logic for plugins and plugin items with link
    """
    link_type = models.CharField(_('link type'), max_length=255, blank=True, null=True,
                                 default=settings.PLEIADI_CMSPLUGINS_LINK_TYPE_DEFAULT_LINK_TYPE,
                                 choices=settings.PLEIADI_CMSPLUGINS_LINK_TYPE_LINK_TYPE_CHOICES,
                                 help_text=_('Which kind of link this item will have.'))
    link_page = PageField(verbose_name=_("page"), null=True, blank=True,
                          help_text=_('For link to page. Select any page from the list.'))

    link_url = models.CharField(_("web address"), max_length=255, blank=True, null=True,
                                help_text=_('For link to web address. Example: Enter "http://www.domain.com" to create '
                                            'an absolute link to an external site, or enter a relative URL like '
                                            '"/about/contact".'))
    link_target = models.CharField(_("link target"), max_length=64, blank=True, null=True, default='_self',
                                   choices=(('_self', _('same window')), ('_blank', _('new window'))))

    class Meta:
        abstract = True

    @property
    def link_to(self):
        """
        Renamed from 'link' to 'link_to' to avoid conflict with link property link of other class (probably CMSPlugin)

        This is the place to put your link priority logic, now the link type is chosen by admin

        @return:
            {
                'href': according to your link-type priority policy, returns:
                        link_url or link_page
                'target': link_target
            }
        """
        link_href = None

        if self.link_type == settings.PLEIADI_CMSPLUGINS_LINK_TYPE_NO_LINK:
            link_href = None

        if self.link_type == settings.PLEIADI_CMSPLUGINS_LINK_TYPE_PAGE_LINK:
            try:
                link_href = self.link_page.get_absolute_url()
            except AttributeError:
                link_href = None

        if self.link_type == settings.PLEIADI_CMSPLUGINS_LINK_TYPE_URL_LINK:
            link_href = self.link_url

        if self.link_type == settings.PLEIADI_CMSPLUGINS_LINK_TYPE_MEDIA_LINK:
            try:
                link_href = self.media.url
            except AttributeError:
                link_href = None

        if link_href:
            link = {
                'href': link_href,
                'target': self.link_target
            }
        else:
            link = {}

        return link


class VideoMixin(models.Model):
    YOUTUBE = 'YT'
    YOUKU = 'YK'
    VIMEO = 'VM'

    VIDEO_TYPE_CHOICES = (
        (YOUTUBE, 'YouTube'),
        (VIMEO, 'Vimeo'),
        (YOUKU, 'YouKu'),
    )

    video_type = models.CharField(_('Video type'), max_length=2,
                                  choices=VIDEO_TYPE_CHOICES, default=YOUTUBE)
    video_id = models.CharField(max_length=200, null=True, blank=True)
    video_width = DimensionField(_('player width'), null=True, blank=True,
                                 help_text=_('You can leave this empty to use an automatically determined image width.'),
                                 default=600)
    video_height = DimensionField(_('player height'), null=True, blank=True,
                                  help_text=_('You can leave this empty to use an automatically determined image height.'
                                              '<br /><strong>Width</strong> is intended to be 100% of the container'),
                                  default=338)

    class Meta:
        abstract = True


# class LayoutMixin(models.Model):
#     """
#     Adds layout properties to a model
#
#     Every model extending LayoutMixin must define his own PLUGIN_TEMPLATES property containing a tuple of template;
#     see settings.PLEIADI_CONTENTS
#
#     This may be a global model since the layout logic should be the same for all the models in the project
#     """
#     CENTER = "center"
#     LEFT = "left"
#     RIGHT = "right"
#     FLOAT_CHOICES = (
#         (CENTER, _("Center")),
#         (LEFT, _("Left")),
#         (RIGHT, _("Right")),
#     )
#
#     PLUGIN_TEMPLATES = (
#         ('pleiadi_cmsplugins/fake.html', u'Fake template'),
#     )
#
#     css_custom = models.CharField(_("css custom class"), max_length=255, null=True, blank=True)
#     float = models.CharField(_("side"), max_length=10, blank=True, null=True, choices=FLOAT_CHOICES)
#     template = models.CharField(_('Template'), max_length=255)
#
#     class Meta:
#         abstract = True


# class AdaptiveMixin(models.Model):
#     FULL = "full"
#     THREE_QUARTER = "three_quarter"
#     HALF = "half"
#     ONE_QUARTER = "one_quarter"
#     WIDTH_CHOICES = (
#         (FULL, _("full")),
#         (THREE_QUARTER, _("three quarter")),
#         (HALF, _("half")),
#         (ONE_QUARTER, _("one quarter")),
#     )
#
#     width = models.CharField(_('Width'), max_length=255, choices=WIDTH_CHOICES, default=FULL)
#
#     class Meta:
#         abstract = True


class ListItemBase(models.Model):
    """
    Base model for all listing plugin item

    All the listing plugins item models (those related to a list plugin model) MUST extend this class or a derived one.
    """
    position = models.IntegerField(_('position'), blank=True, default=0)

    class Meta:
        abstract = True
        ordering = ['position', 'id']
        verbose_name = _('media object')
        verbose_name_plural = _('media objects')

    def save(self, *args, **kwargs):
        if self.position is None:
            self.position = 0
        super(ListItemBase, self).save(*args, **kwargs)

    def __unicode__(self):
        try:
            if self.media.file.size:
                return u'%s %s' % (self.media.name, self.media.file.name)
            else:
                return u'%s %s' % (self.media.name, 'File Not Found')
        except AttributeError:
            return u'List Item'

    def extension(self):
        name, extension = os.path.splitext(self.media.file.name)
        return extension.replace('.', '')


class ListLinkedItemBase(ListItemBase, LinkMixin):
    """
    Base model for all listing plugin item with link
    """
    class Meta:
        abstract = True
        verbose_name = _('media object')
        verbose_name_plural = _('media objects')





