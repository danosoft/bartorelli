import os


class RunEnvironment(object):
    LIVE = 'LIVE'
    TEST = 'TEST'
    LOCAL = 'LOCAL'

    ENV_VAR_NAME = 'RUN_ENVIRONMENT'


    @staticmethod
    def current_env():
        current_env_string = os.environ.get(RunEnvironment.ENV_VAR_NAME)
        if current_env_string:
            class_dict = RunEnvironment.__dict__
            for key in class_dict:
                if class_dict[key] == current_env_string:
                    return current_env_string
        return RunEnvironment.LOCAL

    @staticmethod
    def site_id():
        try:
            return int(os.environ['SITE_ID'])
        except KeyError:
            return 1

    @staticmethod
    def secret_key():
        return 'cyy0hgt-j&n395f#m*-q2$f4s=-xm+wc=&y)zp!l$#8+g6!h)$'