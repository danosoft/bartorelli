from functools import wraps

from django.contrib.sites.models import Site
from django.utils.decorators import available_attrs
from django.views.decorators.cache import cache_page, cache_control
from django.views.decorators.vary import vary_on_headers
from django.conf import settings


def cache_page_by_user(timeout):
    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            return cache_page(timeout, key_prefix="bartorelli_user_sites_%s_%s_" % (Site.objects.get_current().pk, request.user.pk))(view_func)(request, *args, **kwargs)
        return _wrapped_view
    return decorator


def cache_control_and_vary(max_age=None, headers=None):
    """
    Set all the cache control headers in response.

    Used to control cache services like Varnish.

    :param max_age: integer. How long the cache should last, in seconds.
    :param headers: tuple. Headers that invalidated the cache.
    :return:
    """
    max_age = max_age or settings.CACHE_DURATION_DEFAULT
    vary_on_header = headers or settings.VARY_ON_HEADER_DEFAULT

    def decorator(view_func):
        @wraps(view_func, assigned=available_attrs(view_func))
        def _wrapped_view(request, *args, **kwargs):
            return vary_on_headers(*vary_on_header)(cache_control(max_age=max_age)(view_func))(request, *args, **kwargs)
        return _wrapped_view
    return decorator