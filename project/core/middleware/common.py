import logging

from django.contrib.sites.models import Site
from django.conf import settings

logger = logging.getLogger('django.request')


class HostFromSite(object):

    def process_request(self, request):
        if not settings.DEBUG:
            request.META['HTTP_HOST'] = Site.objects.get_current().domain