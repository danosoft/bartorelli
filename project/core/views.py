from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.conf import settings
from django.template.response import TemplateResponse
from django.utils import translation
from django.core.urlresolvers import reverse, NoReverseMatch
from mailing.utils import send_email
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import loader, Context
from django.utils import translation
from django.utils.translation import ugettext_lazy as _
from raven.utils import json
from django.apps import apps
from django.conf import settings
from custom_auth.models import CustomUser
from contacts.models import Contact

def sitemap_index(request):
    # TODO: How to check the sitemap index change date?

    template = 'sitemap_languages_index.xml'

    languages = settings.LANGUAGES
    sitemaps = []

    request_language_code = translation.get_language()

    for code, label in languages:
        locale = code

        if locale:
            translation.activate(code)
            sitemaps.append({
                'url': 'http://%s%s' % (Site.objects.get(), reverse('sitemap_xml'))
            })

    translation.activate(request_language_code)

    context = {
        'items': sitemaps
    }

    return TemplateResponse(request, template, context, content_type='application/xml')


def branch_admin_visibility_js(request):
    user = request.user
    enable_branch_admin_visibility = not user.is_superuser

    # attributes
    app_name = request.GET.get('app_name', '')
    model_name = request.GET.get('model_name', '')
    obj_pk = request.GET.get('obj_pk', '')
    obj_nrprot = request.GET.get('nrprotoade', '')

    language_to_hide = []
    # first_enabled_language = user.first_enabled_language
    #
    # for lang in user.disabled_languages:
    #     language_to_hide.append(lang)

    try:
        notify_changes_url = reverse('contacts:notify_changes', args=[app_name, model_name, obj_pk])
    except NoReverseMatch:
        notify_changes_url = ''
    context = {
        'languages_to_hide': '',
        'initial_language': '',
        'enable_branch_admin_visibility': enable_branch_admin_visibility,
        'app_name': app_name,
        'obj_pk': obj_pk,
        'obj_nrprot': obj_nrprot,
        'model_name': model_name,
        'notify_changes_url': notify_changes_url
    }

    return render_to_response('admin_overrides/branch_admin_visibility/branch_admin_visibility.js', context=context,
                              content_type='application/javascript')


def notify_changes(request, app_name=None, model_name=None, obj_pk=None, nrprotoade=None):
    user = request.user
    current_language = translation.get_language()
    subject = _('Bartorelli Website - Notifica Protocollo AdE')

    Klass = apps.get_model(app_label=app_name, model_name=model_name)
    body_context = {
        'app_name': apps.get_app_config(app_name).verbose_name,
        'edited_content_contr': Klass.objects.get(pk=obj_pk).tiposervizio,
        'edited_content_nrprotoade': request.GET.get('nrprotoade', ''),
        'editor_first_name': Klass.objects.get(pk=obj_pk).first_name,
        'editor_last_name': Klass.objects.get(pk=obj_pk).last_name,
        'author_first_name': user.first_name,
        'author_last_name': user.last_name,
        'author_email': user.email,
    }
    id_email = Klass.objects.get(pk=obj_pk).email

    users_notified = []
    for related_user in user.get_app_related_users(app_name, model_name):
        body_context.update({
            'mail_title': subject,
            'backend_url': request.build_absolute_uri(reverse('admin:index'))
        })

        t = loader.get_template('admin_overrides/branch_admin_visibility/mailing/mail_notify_changes.html')
        body = t.render(Context(body_context))
        send_email(
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipients=[id_email],
            subject=subject,
            body=body,
            replyto=settings.DEFAULT_FROM_EMAIL,
            bcc=['d.caprelli@hoopcommunication.it']
        )
        users_notified.append(related_user.email)

    # metto anche nel messaggio di notifica la mail del tipo
    users_notified.append(id_email)

    warning_message_template = '<li class="warning">Nessun messaggio di notifica inviato</li>'
    ok_message_template = '<li class="success">Messaggio di notifica inviato a: %s</li>'

    message = warning_message_template
    if users_notified:
        message = ok_message_template % ', '.join(users_notified)
        #salvo col nuovo valore di protocollo ade
        contact = Contact.objects.get(pk=obj_pk)
        contact.nrprotoade = request.GET.get('nrprotoade', '')
        contact.save()

    context = {
        'msg': message
    }

    return HttpResponse(json.dumps(context), content_type='application/json')