from django.core.urlresolvers import reverse
from django.shortcuts import render
from paypal.standard.forms import PayPalPaymentsForm

from paypal.standard.models import ST_PP_COMPLETED
from paypal.standard.ipn.signals import valid_ipn_received

from django.conf.urls.static import static
from bartorelli import settings
from django.core.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt
from contacts.models import Contact

# SANDBOX
# https://www.sandbox.paypal.com/signin?country.x=IT&locale.x=it_IT

# Email ID:
# danosoft-facilitator@danosoft.it
# Phone Number:
# 9396126990
# Account Type:
# Business  Upgrade to Pro
# Status:
# Verified
# Country:
# IT
#
#
#
# Classic TEST API Credentials
# Username:
# danosoft-facilitator_api1.danosoft.it
# Password:
# CZ4RFVVCHXZEBLRX
# Signature:
# AFcWxV21C7fd0v3bYYYRCpSSRl31AgQ0x7Gdft5FNq6wS4nREh8AunDk
#
# Bank Account
# Account Number:
# IT10H0542811101739534353999
# Routing Number:
# 05428
# Credit Card
# Credit Card Number:
# 4020020220448381
# Credit Card Type:
# VISA
# Expiration Date:
# 11/2021
# PayPal
# Balance:
# .00 EUR

# BUUYER :
#
# Email ID:
# danosoft-buyer@danosoft.it
# Phone Number:
# 9396289924
# Account Type:
# Personal
# Status:
# Verified
# Country:
# IT
#
# Bank Account
# Account Number:
# IT22J0542811101732688705997
# Routing Number:
# 05428
# Credit Card
# Credit Card Number:
# 4020028120533848
# Credit Card Type:
# VISA
# Expiration Date:
# 11/2021
# PayPal
# Balance:
# 9999.00 EUR

#esempio di ritorno ipn
# mc_gross=19.95&protection_eligibility=Eligible&address_status=confirmed&payer_id=LPLWNMTBWMFAY&tax=0.00&address_street=1+Main+St&payment_date=20%3A12%3A59+Jan+13%2C+2009+PST&payment_status=Completed&charset=windows-1252&address_zip=95131&first_name=Test&mc_fee=0.88&address_country_code=US&address_name=Test+User&notify_version=2.6&custom=&payer_status=verified&address_country=United+States&address_city=San+Jose&quantity=1&verify_sign=AtkOfCXbDm2hu0ZELryHFjY-Vb7PAUvS6nMXgysbElEn9v-1XcmSoGtf&payer_email=gpmac_1231902590_per%40paypal.com&txn_id=61E67681CH3238416&payment_type=instant&last_name=User&address_state=CA&receiver_email=gpmac_1231902686_biz%40paypal.com&payment_fee=0.88&receiver_id=S8XGHLYDW9T3S&txn_type=express_checkout&item_name=&mc_currency=USD&item_number=&residence_country=US&test_ipn=1&handling_amount=0.00&transaction_subject=&payment_gross=19.95&shipping=0.00

# chiamo ipn paypal
@csrf_exempt
def view_that_asks_for_money(request, form, pkuser):

    # What you want the button to do.
    paypal_dict = {
        "business": "danosoft-facilitator@danosoft.it",
        "image_url":  static('core/images/danosoft-inside.jpg'),
        "cpp_logo_image": static('core/images/articolo-paypal.jpg'),
        "amount": form.cleaned_data['calcolodovuto'],
        "item_name": str(dict(form.fields['tiposervizio'].choices)[form.cleaned_data['tiposervizio']]),
        "currency_code": "EUR",
        # "invoice": "keydjango",
        # "notify_url": "http://127.0.0.1:8000/it" + reverse('paypal-ipn'),
        "return_url": settings.URL_PAG_SUB + reverse('show_me_the_money'),
        # "return": "http://127.0.0.1:8000/it" + reverse('show_me_the_money'),
        "rm": "2",
        # "payment_type": "instant",
        "cancel_return": settings.URL_PAG_SUB + reverse('show_me_the_money'),
        "custom": str(pkuser),  # Custom command to correlate to some function later (optional)
    }

    # Create the instance.
    form = PayPalPaymentsForm(initial=paypal_dict)
    context = {"form": form}
    context.update(csrf(request))
    # return render(request, "core/payment.html", context)
    return context

# ritorno da paypal
@csrf_exempt
def show_me_the_money(sender, *args, **kwargs):
    ipn_obj = sender
    # request = kwargs.pop("request")
    if ipn_obj.POST.get('payment_status') == ST_PP_COMPLETED:
        # WARNING !
        # Check that the receiver email is the same we previously
        # set on the business field request. (The user could tamper
        # with those fields on payment form before send it to PayPal)
        if ipn_obj.POST.get('receiver_id') != settings.RECEIVER_ID:
            # Not a valid payment
            context = {
                'payment_status': ipn_obj.POST.get('payment_status'),
                'receiver_email': ipn_obj.POST.get('receiver_email'),
                'amount': ipn_obj.POST.get('mc_gross'),
            }
            return render(ipn_obj, 'core/contact_pagamentono.html', context)

        # ALSO: for the same reason, you need to check the amount
        # received etc. are all what you expect.

        # Undertake some action depending upon `ipn_obj`.
        # upgrade dato del db contatti se pagato o meno
        if ipn_obj.POST.get('custom'):
            # entry = Contact.objects.get(pk=ipn_obj.POST.get('custom'))
            entry = Contact.objects.get(pk=ipn_obj.POST.get('custom'))
            entry.pagato = True
            entry.save()
            context = {
                'payment_status': ipn_obj.POST.get('payment_status'),
                'receiver_email': ipn_obj.POST.get('receiver_email'),
                'amount': ipn_obj.POST.get('mc_gross'),
            }
            return render(ipn_obj, 'core/contact_pagamentosi.html', context)
        #     Users.objects.update(paid=True)
    else:
        context = {
            'payment_status': ipn_obj.POST.get('payment_status'),
            'receiver_email': ipn_obj.POST.get('receiver_email'),
            'amount': ipn_obj.POST.get('mc_gross'),
        }
        return render(ipn_obj, 'core/contact_pagamentono.html', context)

valid_ipn_received.connect(show_me_the_money)