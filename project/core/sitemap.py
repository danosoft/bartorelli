from cms.sitemaps import CMSSitemap
from django.utils import translation


class CMSSitemapByLanguage(CMSSitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        titles = super(CMSSitemapByLanguage, self).items()
        lang = translation.get_language()

        return titles.filter(language=lang)