from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class PleiadiGeographicConfig(AppConfig):
    name = 'pleiadi_geographic'
    verbose_name = _("Geographic")
