"""bartorelli URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.http import HttpResponse
from django.views.generic import TemplateView

from core import RunEnvironment
from core.sitemap import CMSSitemapByLanguage
from core.views import sitemap_index
from products.views import check_import, import_products

urlpatterns = i18n_patterns('',
                            url(r'^admin/', include(admin.site.urls)),
                            url(r'^accounts/', include('custom_auth.urls', namespace='user')),
                            )

if settings.DEBUG:
    urlpatterns = [
                      url(r'^import/$', check_import),
                      url(r'^import/result/$', import_products, name='import_result'),

                      url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                          {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
                      url(r'', include('django.contrib.staticfiles.urls')),
                  ] + urlpatterns

# ==========================================================================
# robots.txt
# ==========================================================================
if RunEnvironment.current_env() == RunEnvironment.LIVE:
    urlpatterns += [
        url(r'^robots\.txt$',
            TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    ]
else:
    urlpatterns += [
        url(r'^robots\.txt$',
            lambda r: HttpResponse("User-agent: *\nDisallow: /", mimetype="text/plain")),
    ]

# ==========================================================================
# sitemap.xml
# ==========================================================================

sitemaps = {
    'cmspages': CMSSitemapByLanguage
}

urlpatterns += [
    url(r'paypal\/payment', 'core.utils.show_me_the_money', name='show_me_the_money'),
]

urlpatterns += [
    url(r'^sitemap\.xml$', sitemap_index, name="sitemap_xml_index"),
]
urlpatterns += i18n_patterns(
    # url(r'^notify/$', include('paypal.standard.ipn.urls'), name='paypal-ipn'),
    url(r'^search/$', TemplateView.as_view(template_name='core/common/google_search.html'),
        name='google-search'),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='sitemap_xml'),
    url(r'^', include('cms.urls')),
)
