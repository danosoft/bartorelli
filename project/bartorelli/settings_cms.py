gettext = lambda s: s

CMS_TEMPLATES = (
    ('cms/pages/homepage.html', gettext('Homepage')),
    ('cms/pages/default.html', gettext('Default page')),
)

# CMS_ENABLED_PLUGINS = [
#     'TextPlugin',
#     'TeaserPlugin',
#     'GalleryPlugin',
# ]
#
# CMS_TEXTONLY_ENABLED_PLUGINS = ['Image']

# CMS_PLACEHOLDER_CONF = {
#     'page_banner_wide': {
#         'plugins': CMS_ENABLED_PLUGINS,
#         'text_only_plugins': CMS_TEXTONLY_ENABLED_PLUGINS,
#         'name': gettext("Banner Wide"),
#     },
#     'above_the_breadcrumb': {
#         'plugins': CMS_ENABLED_PLUGINS,
#         'text_only_plugins': CMS_TEXTONLY_ENABLED_PLUGINS,
#         'name': gettext("Contents Above the Breadcrumb"),
#     },
#     'page_banner': {
#         'plugins': CMS_ENABLED_PLUGINS,
#         'text_only_plugins': CMS_TEXTONLY_ENABLED_PLUGINS,
#         'name': gettext("Banner"),
#     },
#     'page_paragraphs': {
#         'plugins': CMS_ENABLED_PLUGINS,
#         'text_only_plugins': CMS_TEXTONLY_ENABLED_PLUGINS,
#         'name': gettext("Main Content Area"),
#     },
#     'page_gallery': {
#         'plugins': CMS_ENABLED_PLUGINS,
#         'text_only_plugins': CMS_TEXTONLY_ENABLED_PLUGINS,
#         'name': gettext("Page Image Gallery"),
#     },
#     'page_related': {
#         'plugins': CMS_ENABLED_PLUGINS,
#         'text_only_plugins': CMS_TEXTONLY_ENABLED_PLUGINS,
#         'name': gettext("Related Contents"),
#     },
# }

CMS_LANGUAGES = {

    'default': {
        'fallbacks': [],
        'redirect_on_fallback': True,
        'public': True,
        'hide_untranslated': True,
    }
}

