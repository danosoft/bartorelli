from django.contrib.auth.forms import UserCreationForm, PasswordResetForm, UserChangeForm
from custom_auth.models import CustomUser
from django import forms


class Registration(UserCreationForm):
    email = forms.EmailField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    privacy_ok = forms.BooleanField(required=True)

    class Meta:
        model = CustomUser
        fields = ('username', 'first_name', 'last_name', 'email', 'privacy_ok',)


class ProfilePasswordResetForm(PasswordResetForm):
    pass


class ProfileChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ("username", "first_name", "last_name", "email",)

    def clean_password(self):
        """
        password is not in the form so I can pass the validation
        """
        pass
