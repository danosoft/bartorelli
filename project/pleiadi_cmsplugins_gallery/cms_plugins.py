from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin

from models import PleiadiGallery, PleiadiGalleryItem
from pleiadi_cmsplugins_base.cms_plugins import ListBasePlugin, LinkMixinPlugin


class GalleryBasePlugin(ListBasePlugin):
    """
    Extend the ListBasePlugin handling all the specific configuration fields
    """
    fieldsets = (
        (None, {
            'fields': ('style',)
        }),
        (_('Container extended data'), {
            'classes': ('collapse',),
            'fields': ('title', 'description', 'media')
        }),
        (_('Slideshow only'), {
            'classes': ('collapse',),
            'fields': ('timeout', 'speed', 'fx', 'autoplay', 'show_navigator', 'show_arrows', 'show_thumb', )
        }),
        (_('geometry'), {
            'classes': ('collapse',),
            'fields': ('height', )
        })
    )

    def render(self, context, instance, placeholder):
        context = super(GalleryBasePlugin, self).render(context, instance, placeholder)

        self.render_template = getattr(instance, 'template', 'pleiadi_cmsplugins_gallery/gallery_empty_fake.html')

        context.update({
            # slideshow
            'timeout': instance.timeout,
            'fade': instance.fade,
            'speed': instance.speed,
            'show_dots': instance.show_navigator,
            'show_arrows': instance.show_arrows,
            'show_thumb': instance.show_thumb,
            'autoplay': instance.autoplay,

            # geometry
            'width': instance.width,
            'height': instance.height,
            'dimension': instance.dimension,
        })

        return context


class GalleryItemInline(LinkMixinPlugin, admin.StackedInline):
    model = PleiadiGalleryItem
    extra = 0

    fieldsets = (
        (None, {
            'fields': (('media', 'alt', ), 'position', )
        }),
        ('link', {
            'classes': ('collapse',),
            'fields': (('link_type', 'link_target'), 'link_page', 'link_url')
        }),
        ('Text contents', {
            'classes': ('collapse',),
            'fields': ('title', 'sub_title', 'description')
        }),
    )


class GalleryPlugin(GalleryBasePlugin):
    """
    The actual Gallery Plugin
    """
    name = _("Gallery")
    model = PleiadiGallery

    inlines = [GalleryItemInline]
    render_template = 'pleiadi_cmsplugins_gallery/gallery_empty_fake.html'

plugin_pool.register_plugin(GalleryPlugin)